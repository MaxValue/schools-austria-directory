# Crawler for Austrian School

Selenium web crawler which downloads all Austrian schools from the official web index.

## Contents
[TOC]

## Getting Started

These instructions will get you a copy of the project up and running on your local machine for development and testing purposes. See deployment for notes on how to deploy the project on a live system.

### Prerequisites

What things you need to install the software and how to install them

I recommend you to use the `setup_development_environment.sh` script by running

```
chmod +x setup_development_environment.sh && ./setup_development_environment.sh
```

but if you don't want to do that, here is the complete list of dependencies:

* [Python 3](https://www.python.org/downloads/)
* [Selenium](https://scrapy.org/)
* [Geckodriver](https://github.com/mozilla/geckodriver/releases)
* [Mozilla Firefox](https://www.mozilla.org/en-US/firefox/new/) - The web browser itself

## Running

1. Activate the virtual environment

2. Run the `downloader.py` script. There are additional options explained by the `--help` option.

## Built With

* [Sublime Text 3](https://www.sublimetext.com/) - The code editor I use
* [Python 3](https://www.sublimetext.com/) - For running the dev web server

## Contributing

Please open an issue on the online repository if you want to add/change something.

### Roadmap
Things I already plan to implement, but didn't have yet:
- [ ] click CLI library instead of argparse
- [ ] better requirements
- [ ] Better export features

## Versioning

We use [SemVer](http://semver.org/) for versioning. For the versions available, see the tags on this repository.

## Authors

* **Max Fuxjäger** - *Initial work* - [MaxValue](https://gitlab.com/MaxValue)

## License

This project is licensed under the MIT License - see the [LICENSE.txt](LICENSE.txt) file for details.

## Acknowledgments

* **Federico Leva** - *pushed me to release this finally* - [Website](https://federicoleva.eu/)

### Project History
This project was created because I (Max) wanted to check the addresses of the schools against other public databases.
