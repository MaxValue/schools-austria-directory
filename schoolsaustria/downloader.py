#!/usr/bin/env python3
# coding: utf-8

'''
add typical arguments
run simple test
'''

from selenium import webdriver
from selenium.webdriver.support import expected_conditions as selenium_webdriver_support_expectedConditions
from selenium.common.exceptions import TimeoutException as selenium_common_exceptions_TimeoutException
from selenium.common.exceptions import ElementNotInteractableException as selenium_common_exceptions_ElementNotInteractableException
from selenium.common.exceptions import ElementClickInterceptedException as selenium_common_exceptions_ElementClickInterceptedException
import databaseSchema2 as dbSchema
import sqlalchemy as sqla
import argparse, time, os, re, urllib, datetime, logging

parser = argparse.ArgumentParser(argument_default=False, description='Download contact data of all schools of austria.')
parser.add_argument('database', nargs='?', default='results.db', help='The location of the database.')
parser.add_argument('--verbose', '-v', action='count', default=0, help='Turn on verbose mode.')
parser.add_argument('--log', help='Logfile path. If omitted, stdout is used.')
parser.add_argument('--debug', '-d', action='store_true', help='Log all messages including debug.')
parser.add_argument('-p', '--profile', default='browser_profiles/firefox', help='Path to a valid Firefox profile if you wanna use a specially prepared profile.')
parser.add_argument('--wait-after-newtab', default=0, type=int, help='Whether to wait after a new tab is opened and for how many seconds.')
parser.add_argument('--newtab-url', default='about:blank', help='What page should be initially loaded when a new tab is opened.')
args = parser.parse_args()

if args.debug:
    loglevel = logging.DEBUG
elif args.verbose:
    loglevel = logging.INFO
else:
    loglevel = logging.WARNING

if args.log:
    logging.basicConfig(filename=args.log, filemode='a', level=loglevel)
else:
    logging.basicConfig(level=loglevel)

logging.info('loading configs')

def open_newtab(url=args.newtab_url, delay=0.05, max_tries=100):
    global browser
    '''Returns the window handle of the new tab.'''
    windows_before = set(browser.window_handles)
    browser.execute_script('window.open("{}", "_blank");'.format(url))
    for i in range(max_tries):
        new_handles = windows_before ^ set(browser.window_handles)
        if len(new_handles) == 0:
            time.sleep(delay)
        else:
            return new_handles.pop()

def wait_for_element(xpath, timeout=600, base=False):
    if not base:
        base = browser
    try:
        selection = webdriver.support.ui.WebDriverWait(base, timeout).until(
            selenium_webdriver_support_expectedConditions.presence_of_all_elements_located(
                (webdriver.common.by.By.XPATH,xpath)
            )
        )
    except:
        return False
    return selection

def select_dropdown_option(xpath_dropdown, option_selector, fallback_text, verification_selector, timeout=60):
    global browser
    dropdown = browser.find_element_by_xpath(xpath_dropdown)
    try:
        if type(dropdown) == webdriver.firefox.webelement.FirefoxWebElement:
            dropdown.click()
            dropdown_option = browser.find_element_by_xpath(option_selector)
            if type(dropdown_option) == webdriver.firefox.webelement.FirefoxWebElement:
                dropdown_option.click()
            else:
                input(fallback_text)
    except (selenium_common_exceptions_ElementNotInteractableException, selenium_common_exceptions_ElementClickInterceptedException) as e:
        input(fallback_text)
    verification_element = wait_for_element(verification_selector, timeout)
    if verification_element:
        return True
    else:
        return False

def select_checkbox(checkbox_selector, fallback_text, verification_selector, timeout=60):
    global browser
    checkbox = browser.find_element_by_xpath(checkbox_selector)
    try:
        if type(checkbox) == webdriver.firefox.webelement.FirefoxWebElement:
            checkbox.click()
        else:
            input(fallback_text)
    except (selenium_common_exceptions_ElementNotInteractableException, selenium_common_exceptions_ElementClickInterceptedException) as e:
        input(fallback_text)
    verification_element = wait_for_element(verification_selector, timeout)
    if verification_element:
        return True
    else:
        return False

def click_on_element(element):
    browser.execute_script('arguments[0].scrollIntoView();', element)
    try:
        webdriver.common.touch_actions.TouchActions(browser).scroll(element.rect['x'], element.rect['y'])
    except:
        pass
    try:
        webdriver.common.action_chains.ActionChains(browser).move_to_element(element).click().perform()
        # element.click()
    except:
        try:
            webdriver.common.action_chains.ActionChains(browser).move_to_element(element).send_keys(key_Return).perform()
            # element.send_keys(key_Return)
        except:
            return False
    return True

def closeDB():
    dbSession.commit()
    dbSession.close()

URL_initialPage = 'https://www.schulen-online.at/sol/index.jsf'

xpath_tab1 = '//div[@id="tabs"]/ul[@id="tabAnzeige"]/li/a[@href="#tabs-1"][contains(text(),"Auswahl")]'
xpath_tab2 = '//div[@id="tabs"]/ul[@id="tabAnzeige"]/li/a[@href="#tabs-2"][contains(text(),"Ergebnis")]'
xpath_tab3 = '//div[@id="tabs"]/ul[@id="tabAnzeige"]/li/a[@href="#tabs-3"][contains(text(),"Detailansicht")]'
xpath_nextPage = '//a[@id="j_id_20:next"]'

xpath_frontPageTest = '//div[@id="welcome"]'
xpath_button_Schulsuche = '//a[@id="j_id_c:sucheSchulen"]'

xpath_searchPageTest = '//input[@id="myform1:skz"]'
xpath_button_searchstart = '//input[@id="myform1:j_id_1x"]'

xpath_searchResultsTest = '//div[@class="rahmen_list"]/table[@class="ergebnisTable"]'
xpath_searchResults = '//tbody[@id="j_id_20:j_id_22:tbody_element"]/tr/td[@class="c1"]/div[@class="skz"]/a'

xpath_result_dataContainers = '//form[@id="myform3"]/div[@class="rahmen_tab"]/div[@class="anzeigefeld"]'
xpath_result_dataField = 'div/h5[text()]/following-sibling::div[1]'
xpath_result_dataFieldTitle = 'preceding-sibling::h5[1]'

fields = {
    'Schulkennzahl': 'skz',
    'Titel': 'title',
    'Adresse': 'postalAddress',
    'öffentlich/privat': 'publicPrivate',
    'Schulerhalter': 'schulerhalter',
    'Schulbehörde 1. Instanz': 'schoolAuthority1Instance',
    'Schulart(en)': 'type',
    'Schulische Tagesbetreuung': 'tagesbetreuung',
    'Telefon': 'contactPhone',
    'Fax': 'contactFax',
    'E-Mail Verwaltung': 'contactAdministrationEMail',
    'E-Mail Pädagogik': 'contactEducationEMail',
    'Homepage': 'website',
}

key_Return = webdriver.common.keys.Keys.RETURN

logging.info('Connecting to database')
dbSession = dbSchema.init_session(args.database)
spider = dbSession.query(dbSchema.Spider).filter(dbSchema.Spider.project=='schoolsaustria').filter(dbSchema.Spider.name=='schools_selenium').first()
if not spider:
    spider = dbSchema.Spider(project='schoolsaustria', name='schools_selenium')
    dbSession.add(spider)
# db_crawlingJob = dbSchema.Job(timeStart=datetime.datetime.now(), spider=spider)
db_crawlingJob = dbSession.query(dbSchema.Job).filter(dbSchema.Job.id==1).first()
# dbSession.add(db_crawlingJob)
dbSession.commit()
logging.info('Job ID is: %d', db_crawlingJob.id)

try:
    logging.info('configuring browser options')
    options = webdriver.FirefoxOptions()
    if args.profile:
        profile = webdriver.FirefoxProfile(profile_directory=args.profile)
        options._profile = profile
    options.set_preference('browser.link.open_newwindow',3)
    options.set_preference('browser.link.open_newwindow.override.external',3)
    options.set_preference('browser.link.open_newwindow.restriction',0)
    logging.info('starting browser')
    if args.profile:
        browser=webdriver.Firefox(firefox_profile=profile, options=options)
    else:
        browser=webdriver.Firefox(options=options)

    # Initial Page
    logging.info('Loading initial page')
    browser.get(URL_initialPage)
    isFrontpageLoaded = wait_for_element(xpath_frontPageTest)
    if not isFrontpageLoaded:
        closeDB()
        exit('Could not load front page.')

    # Going to search form
    logging.info('Trying to click on school search')
    button_Schulsuche = wait_for_element(xpath_button_Schulsuche)
    if button_Schulsuche:
        logging.info('Clicking on school search')
        click_on_element(button_Schulsuche[0])
    isSearchPageLoaded = wait_for_element(xpath_searchPageTest)
    if not isSearchPageLoaded:
        closeDB()
        exit('Could not load search form.')

    # Filling in search form
    logging.info('Setting result size to 50')
    select_dropdown_option(
        '//select[@id="myform1:anz"]',
        '//select[@id="myform1:anz"]/option[@value="50"]',
        'Please select "50" as result size in "Treffer pro Seite".',
        '//select[@id="myform1:anz"]/option[@value="50"][@selected="selected"]'
    )

    # Submitting search
    logging.info('Trying to search form')
    button_SucheStarten = wait_for_element(xpath_button_searchstart)
    if button_SucheStarten:
        logging.info('Submitting search form')
        click_on_element(button_SucheStarten[0])

    reachedLastPage = False

    while not reachedLastPage:

        searchResultsPageFinished = False

        while not searchResultsPageFinished:

            time.sleep(1)

            button_tab2 = wait_for_element(xpath_tab2)
            if button_tab2:
                click_on_element(button_tab2[0])

            isSearchresultsLoaded = wait_for_element(xpath_searchResultsTest)
            if not isSearchresultsLoaded:
                closeDB()
                exit('Could not load search results.')

            time.sleep(1)

            searchResults = wait_for_element(xpath_searchResults)
            for searchResult in searchResults:
                skz = searchResult.text.strip()
                existingItem = dbSession.query(dbSchema.Item)\
                    .filter(dbSchema.Item.job==db_crawlingJob)\
                    .filter(dbSchema.Item.skz==skz)\
                    .first()
                if not existingItem:
                    newItem = dbSchema.Item(job=db_crawlingJob, skz=skz)
                    dbSession.add(newItem)
                    dbSession.commit()
            for searchResult in searchResults:
                skz = searchResult.text.strip()
                incompleteItem = dbSession.query(dbSchema.Item)\
                    .filter(dbSchema.Item.skz==skz)\
                    .filter(dbSchema.Item.scrapingStatus==0)\
                    .first()
                if incompleteItem:
                    click_on_element(searchResult)
                    logging.info('######################################')
                    time.sleep(1)
                    dataContainers = wait_for_element(xpath_result_dataContainers)
                    if dataContainers:
                        logging.info('\tSKZ: {} - Going through data containers.'.format(skz))
                        for dataContainer in dataContainers:
                            logging.info('\tSKZ: {} - Getting data container title.'.format(skz))
                            dataContainer_titleElems = wait_for_element('h4[1]', base=dataContainer)
                            if dataContainer_titleElems:
                                dataContainer_title = dataContainer_titleElems[0].text
                                if dataContainer_title == 'Grunddaten':
                                    logging.info('\tSKZ: {} - Going through Grunddaten.'.format(skz))
                                    dataFields = wait_for_element(xpath_result_dataField, base=dataContainer)
                                    if dataFields:
                                        for dataField in dataFields:
                                            dataFieldValue = dataField.text
                                            dataFieldTitleElems = wait_for_element(xpath_result_dataFieldTitle, base=dataField)
                                            if dataFieldTitleElems:
                                                dataFieldTitle = dataFieldTitleElems[0].text
                                                logging.info('\t\t{}: {}'.format(dataFieldTitle, dataFieldValue))
                                                if dataFieldTitle in fields:
                                                    setattr(incompleteItem, fields[dataFieldTitle], dataFieldValue)
                                                else:
                                                    logging.error('\tSKZ: {} - Unknown data field "{}"!'.format(skz, dataFieldTitle))
                                                    incompleteItem.scrapingStatus = 2
                                                if dataFieldTitle == 'SKZ' and dataFieldValue!=skz:
                                                    logging.error('\tSKZ: {} - Number is not same!'.format(skz))
                                                    incompleteItem.scrapingStatus = 2
                                            else:
                                                logging.error('\tSKZ: {} - No data field title found.'.format(skz))
                                                incompleteItem.scrapingStatus = 2
                                    else:
                                        logging.error('\tSKZ: {} - No data fields in Grunddaten container.'.format(skz))
                                        incompleteItem.scrapingStatus = 2
                                elif dataContainer_title == 'Kontakt':
                                    logging.info('\tSKZ: {} - Going through Kontakt.'.format(skz))
                                    dataFields = wait_for_element(xpath_result_dataField, base=dataContainer)
                                    if dataFields:
                                        for dataField in dataFields:
                                            dataFieldValue = dataField.text
                                            dataFieldTitleElems = wait_for_element(xpath_result_dataFieldTitle, base=dataField)
                                            if dataFieldTitleElems:
                                                dataFieldTitle = dataFieldTitleElems[0].text
                                                logging.info('\t\t{}: {}'.format(dataFieldTitle, dataFieldValue))
                                                if dataFieldTitle in fields:
                                                    setattr(incompleteItem, fields[dataFieldTitle], dataFieldValue)
                                                else:
                                                    logging.error('\tSKZ: {} - Unknown data field "{}"!'.format(skz, dataFieldTitle))
                                                    incompleteItem.scrapingStatus = 2
                                                if dataFieldTitle == 'SKZ' and dataFieldValue!=skz:
                                                    logging.error('\tSKZ: {} - Number is not same!'.format(skz))
                                                    incompleteItem.scrapingStatus = 2
                                            else:
                                                logging.error('\tSKZ: {} - No data field title found.'.format(skz))
                                                incompleteItem.scrapingStatus = 2
                                    else:
                                        logging.error('\tSKZ: {} - No data fields in Grunddaten container.'.format(skz))
                                        incompleteItem.scrapingStatus = 2
                                else:
                                    logging.warning('\tSKZ: {} - Encountered additional data container "{}".'.format(skz,dataContainer_title))
                                    incompleteItem.scrapingStatus = 2
                            else:
                                logging.error('\tSKZ: {} - No container title found!'.format(skz))
                                incompleteItem.scrapingStatus = 2
                    else:
                        closeDB()
                        exit('\tSKZ: {} - Could not load result details.'.format(skz))
                    if incompleteItem.scrapingStatus != 2:
                        incompleteItem.scrapingStatus = 1
                    dbSession.commit()
                    break
            else:
                searchResultsPageFinished = True
        # click on next page
        button_nextPage = wait_for_element(xpath_nextPage)
        if button_nextPage:
            click_on_element(button_nextPage[0])
        else:
            reachedLastPage = True

    browser.quit()
    closeDB()
    print('Finished!')
except KeyboardInterrupt as e:
    browser.quit()
    closeDB()
    exit()
