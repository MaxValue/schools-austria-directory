# -*- coding: utf-8 -*-

import sqlalchemy as sqla
from sqlalchemy.ext.declarative import declarative_base as sqla_declarative_base

Base = sqla_declarative_base()

class Spider(Base):
	'''Used to declare a spider'''
	__tablename__ = 'spiders'
	id = sqla.Column('id', sqla.Integer, primary_key=True)
	project = sqla.Column('project', sqla.String)
	name = sqla.Column('name', sqla.String, nullable=False)

class FinishReason(Base):
	'''Used to declare a finishing reason'''
	__tablename__ = 'finishingreasons'
	id = sqla.Column('id', sqla.Integer, primary_key=True)
	name = sqla.Column('name', sqla.String, nullable=False)
	description = sqla.Column('description', sqla.String)

class Job(Base):
	'''Used to declare a crawling job'''
	__tablename__ = 'jobs'
	id = sqla.Column('id', sqla.Integer, primary_key=True)
	finishingReason_id = sqla.Column('finishingReason_id', None, sqla.ForeignKey('finishingreasons.id'))
	spider_id = sqla.Column('spider_id', None, sqla.ForeignKey('spiders.id'), nullable=False)
	timeStart = sqla.Column('timeStart', sqla.TIMESTAMP, nullable=False)
	timeEnd = sqla.Column('timeEnd', sqla.TIMESTAMP)

	finishingReason = sqla.orm.relationship('FinishReason', back_populates='jobs')
	spider = sqla.orm.relationship('Spider', back_populates='jobs')

FinishReason.jobs = sqla.orm.relationship('Job', order_by=Job.timeStart, back_populates='finishingReason')
Spider.jobs = sqla.orm.relationship('Job', order_by=Job.timeStart, back_populates='spider')

def init_session(database_path, echo_commands=False):
	full_database_path = 'sqlite+pysqlite:///{}'.format(database_path)
	engine = sqla.create_engine(full_database_path, echo=echo_commands)
	Base.metadata.create_all(engine)
	Session = sqla.orm.sessionmaker(bind=engine)
	session = Session()
	return session
