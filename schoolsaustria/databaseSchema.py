# -*- coding: utf-8 -*-

import sqlalchemy as sqla
from databaseSchemaBase import *

class Item(Base):
    '''Used to declare an item'''
    __tablename__ = 'items'
    id = sqla.Column('id', sqla.Integer, primary_key=True)
    job_id = sqla.Column('job_id', None, sqla.ForeignKey('jobs.id'))
    skz = sqla.Column('SKZ', sqla.String, nullable=False)
    title = sqla.Column('Title', sqla.String)
    postalAddress = sqla.Column('Postal Address', sqla.String)
    publicPrivate = sqla.Column('Public/Private', sqla.String)
    schulerhalter = sqla.Column('Schulerhalter', sqla.String)
    schoolAuthority1Instance = sqla.Column('School Authority 1. Instance', sqla.String)
    type = sqla.Column('Type', sqla.String)
    tagesbetreuung = sqla.Column('Tagesbetreuung', sqla.String)
    contactPhone = sqla.Column('Contact Phone', sqla.String)
    contactFax = sqla.Column('Contact Fax', sqla.String)
    contactAdministrationEMail = sqla.Column('Contact Administration E-Mail', sqla.String)
    contactEducationEMail = sqla.Column('Contact Education E-Mail', sqla.String)
    website = sqla.Column('Website', sqla.String)
    scrapingStatus = sqla.Column('Scraping Status', sqla.Integer, nullable=False, default=0)

    job = sqla.orm.relationship('Job', back_populates='items')

Job.items = sqla.orm.relationship('Item', order_by=Item.id, back_populates='job')

def init_session(database_path, echo_commands=False):
	full_database_path = 'sqlite+pysqlite:///{}'.format(database_path)
	engine = sqla.create_engine(full_database_path, echo=echo_commands)
	Base.metadata.create_all(engine)
	Session = sqla.orm.sessionmaker(bind=engine)
	session = Session()
	return session
